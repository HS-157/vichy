#!/usr/bin/env python3

import tkinter as tk

class Canvas:

    def __init__(self, size, nb):
        self.size = size
        self.nb = nb
        self.window = tk.Tk()
        self.canvas = tk.Canvas(self.window, width=self.size, height=self.size, background='black')
        self.size_tile = self.size / self.nb
        self.tiles()
        self.lines()

    def draw(self):
        self.canvas.pack()
        self.window.mainloop()

    def tiles(self):
        pos = [ (x,y) for x in range(self.nb) for y in range(self.nb) ]
        self.tiles = [ Tile(self, p) for p in pos ]

    def lines(self):
        self.lines = []
        
        for i in range(nb):
            self.lines.append(Line(self, "C", i, self.tiles))
            self.lines.append(Line(self, "L", i, self.tiles))
            if i < 2:
                self.lines.append(Line(self, "D", i, self.tiles))

class Tile:

    def __init__(self, canvas, pos):
        self.canvas = canvas
        self.size = self.canvas.size_tile
        self.pos = pos
        self.x = pos[0]
        self.y = pos[1]
        self.canvas_x = (self.x + 1) * self.size
        self.canvas_y = (self.y + 1) * self.size
        self.middle_x = (self.canvas_x * 2 - self.size) / 2
        self.middle_y = (self.canvas_y * 2 - self.size) / 2
        self.set_color()
        self.draw()
        self.draw_pos()

    def __str__(self):
        return f"Tile : {(self.x, self.y)} - Size : {round(self.size)} - Pos : {(round(self.middle_x), round(self.middle_y))}"
        
    def set_color(self):
        if False not in [ True if p % 2 == 0 else False for p in self.pos ]:
            self.color = "gray35"
        elif False not in [ True if p % 2 == 1 else False for p in self.pos ]:
            self.color = "gray35"
        else:
            self.color = "gray25"

    def draw(self):
        x1 = self.canvas_x - self.size
        y1 = self.canvas_y - self.size
        x2 = self.canvas_x
        y2 = self.canvas_y

        self.canvas.canvas.create_rectangle(x1, y1, x2, y2, fill=self.color, outline="black")

    def draw_pos(self):
        size = round(self.size / 5)
        self.canvas.canvas.create_text(self.middle_x, self.middle_y, text=f"({self.x},{self.y})", font=f"Arial {size}", fill="white")


class Line:

    def __init__(self, canvas, type_line, nb, tiles):
        self.canvas = canvas
        self.type = type_line
        self.nb = nb
        self.set_color()
        self.get_tiles(tiles)
        self.get_min()
        self.get_max()
        self.draw()

    def get_tiles(self, tiles):
        if self.type == "C":
            self.tiles = [ t for t in tiles if t.y == self.nb ]
        elif self.type == "L":
            self.tiles = [ t for t in tiles if t.x == self.nb ]
        elif self.type == "D" and self.nb == 0:
            self.tiles = [ t for t in tiles if t.x == t.y ]
        elif self.type == "D" and self.nb == 1:
            self.tiles = [ t for t in tiles if t.x + t.y == self.canvas.nb - 1 ]

    def draw(self):
        self.canvas.canvas.create_line(self.min.middle_x, self.min.middle_y, self.max.middle_x, self.max.middle_y, fill=self.color)
        # for tile in self.tiles:
        #     middle_x = tile.middle_x
        #     middle_y = tile.middle_y
        #     self.canvas.canvas.create_rectangle(middle_x + 5, middle_y + 5, middle_x - 5, middle_y - 5, fill=self.color, outline="black")

    def set_color(self):
        if self.type == "C":
            self.color = "green"
        elif self.type == "L":
            self.color = "yellow"
        elif self.type == "D" and self.nb == 0:
            self.color = "red"
        elif self.type == "D" and self.nb == 1:
            self.color = "blue"

    def tile_min(self, tile):
        if self.type == "C":
            return tile.x
        elif self.type == "L":
            return tile.y
        elif self.type == "D" and self.nb == 0:
            return (tile.x, tile.y)
        elif self.type == "D" and self.nb == 1:
            return tile.x + tile.y

    def get_min(self):
        self.min = min(self.tiles, key=self.tile_min)
        middle_x = self.min.middle_x
        middle_y = self.min.middle_y
        # self.canvas.canvas.create_rectangle(middle_x + 10, middle_y + 10, middle_x - 10, middle_y - 10, fill="black", outline="black")

    def tile_max(self, tile):
        if self.type == "C":
            return tile.x
        elif self.type == "L":
            return tile.y
        elif self.type == "D" and self.nb == 0:
            return (tile.x, tile.y)
        elif self.type == "D" and self.nb == 1:
            return tile.x

    def get_max(self):
        self.max = max(self.tiles, key=self.tile_max)
        middle_x = self.max.middle_x
        middle_y = self.max.middle_y
        # self.canvas.canvas.create_rectangle(middle_x + 10, middle_y + 10, middle_x - 10, middle_y - 10, fill="orange", outline="black")



size = 2**10
nb = 5

c = Canvas(size, nb)
c.draw()
