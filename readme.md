# Vichy

Un simple script qui génère un damier, affiche les coordonnées et trace les lignes des différentes lignes, colonnes et diagonal avec TK.

![screenshot](screenshot.png)
